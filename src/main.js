import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

Vue.config.productionTip = false
Vue.prototype.$active = []
Vue.prototype.$index = 1
Vue.prototype.$increase = function(){
  this.$index++
  console.log('test')
}
Vue.mixin({
  data: function() {
    return {
      globalnav : []
    }
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
