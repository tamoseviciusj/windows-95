export default {
  mounted() {
    var aboutMe =
      "I am experienced to use coding languages such as PHP, C#, JavaScript,  SQL. Use frameworks - Laravel,  VUE JS, ASP.NET. I have Umbraco CMS certification and I can also work with WordPress CMS. in addition I am  comfortable in SASS, GIT, NPM, Node JS, BEM, working with MVC pattern, PWA, building and using API’s. I also have experience working with  cross platform mobile application development using Xamarin. With my current and continuously developing skills I believe, that I would be able to solve diversity of assignments given to me in the professional and creative way. \n\r My Skills : HTML5 | JAVASCRIPT | C# | ASP.NET | MVC | SQL | VUE.JS | Xamarin | BUILD AND USE API | JQUERY | BOOTSTRAP | WORDPRESS | UMBRACO | PHP | LARAVEL | SASS | GIT | SEO | BEM | NPM | NODE JS | PWA";
    aboutMe.replace(/\r?\n/g, "<br />");
    function Folder(
      name,
      icon,
      type,
      folders,
      text,
      navigation = false,
      desktop = true,
      title,
      description,
      technologies,
      github,
      website,
      link
    ) {
      (this.name = name),
        (this.icon = icon),
        (this.type = type),
        (this.link = link),
        (this.title = title);
      this.description = description;
      this.technologies = technologies;
      this.github = github;
      this.website = website;
      (this.navigation = navigation), (this.desktop = desktop);
      this.active = function(a) {
        return a;
      };

      if ((type = "f")) {
        this.folders = folders;
      }
      if ((type = "t")) {
        this.text = text;
      }
    }
    var piratedesc =
      "This is a multiplayer web browser educational game about Denmark. Players are able to sign up and play against oponents (1v1). Iside the match player answers the questions and if he does it correctly he is able to make a move inside the map and then passed the turn to his opponent. The winner is the player who first crosses the finish line, winner receives in game currency - gold. With gold players can buy new looking boats, change their username.";
    var piratestech =
      "HTML5 CANVAS, AJAX, PHP, MYSQL, JAVASCRIPT, jQUERY, jQUERY VALIDATOR";
    var startupdesc =
      "This project is created for Business Kolding company. In this project entrepreneurs from Kolding and other close cities can take loan or scolarship up to 4 000 000 DKK, by describing their business idea. People with the most unique ideas are abe to recaive this money.";
    var startuptech =
      "WordPress, Custum built WP plugins, AJAX, PHP, MYSQL, JAVASCRIPT, jQUERY, SASS";
    var gviskudzoodesc =
      "Web application that includes an interac- tive map, calendar and animal browser. Made using Umbraco CMS so emploees could update - news, map, calendar";
    var gviskudzootech =
      "C#, Created API, Used API, UMBRACO CMS, ASP.NET MVC, JAVASCRIPT, SASS";
    var portfoliodesc = "Previus portfolio website";
    var portfoliotech = "PHP, SASS, JavaScript, MYSQL";
    var politicsdesc =
      'This project is for Lithuanian people. Here you can find Lithuanian politics and vote for them. This is interactive and funny way to vote and to see others opinion. In the website it is possible to find TOP 10 best and worst politicians and there is section "Visi politikai" - all politicians you can type specific politician name and it will show in which place and home many votes he has. This project was very popular - website had over 10 000 visitors already!';
    var politicstech = "PHP, MYSQL, AJAX, JAVASCRIPT, jQUERY, SASS";
    var firstvuedesc =
      "This is my first VUE JS project with all its features - routing, coditional statemnts, mixins etc.";
    var firstvuetech = "VUE JS, JAVASCRIPT, API, SASS";

    function programs() {
      const InternetIdeas = new Folder(
        "Internet Ideas",
        "project",
        "p",
        [],
        "",
        false,
        true,
        "Internet Ideas",
        "The blogging website, of the international company used to write news. This website was built using WordPress CMS - for this I build cutom WP theme and some custom plugins.",
        "WORDPRESS, MYSQL, SASS, JAVASCRIPT, API",
        "https://gitlab.com/tamoseviciusj/simonas-wordpress-theme",
        "https://internetideas.com/blog/"
      );
      const musudarbas = new Folder(
        "Musu Darbas",
        "project",
        "p",
        [],
        "",
        false,
        true,
        "Musu Darbas (in progress)",
        "Musu Darbas - is an complicated HR managment system which is used to create job postings. People can apply for jobs with an ease, and the job poster has the abbility to manage all the applicants with HR system. On top of everything it also has administrating system which is used to manage all the users and jobs posts for the administrators.",
        "REACT.JS, STRAPI.IO (Headless CMS), API",
        "http://musudarbas.lt/",
        "https://gitlab.com/tobi8678/musudarbas"
      );
      const codeddesign = new Folder(
        "Coded Design",
        "project",
        "p",
        [],
        "",
        false,
        true,
        "Coded Design",
        "Simply coded design",
        "HTML, SASS, JAVASCRIPT",
        "https://github.com/jonas2422/piratewars",
        "http://piratewars.jonas95.com"
      );
      const project = new Folder(
        "Pirate Wars",
        "project",
        "p",
        [],
        "",
        false,
        true,
        "Pirate Wars",
        piratedesc,
        piratestech,
        "https://github.com/jonas2422/piratewars",
        "http://piratewars.jonas95.com"
      );
      const startup = new Folder(
        "StartUp Boost",
        "project",
        "p",
        [],
        "",
        false,
        true,
        "Pirate Wars",
        startupdesc,
        startuptech,
        "",
        "http://startupboost.dk/"
      );
      const gviskudzoo = new Folder(
        "Givskud Website",
        "project",
        "p",
        [],
        "",
        false,
        true,
        "Givskud Zoo",
        gviskudzoodesc,
        gviskudzootech,
        "https://github.com/jonas2422/zoo",
        "http://group518.webq.dk"
      );
      const portfolio = new Folder(
        "Portfolio",
        "project",
        "p",
        [],
        "",
        false,
        true,
        "Old portfolio",
        portfoliodesc,
        portfoliotech,
        "https://github.com/jonas2422/oldportfolio",
        "http://portfolio.jonas95.com/"
      );
      const politics = new Folder(
        "Politics",
        "project",
        "p",
        [],
        "",
        false,
        true,
        "Real Ratings",
        politicsdesc,
        politicstech,
        "https://github.com/jonas2422/politicsgame",
        "http://politics.jonas95.com"
      );
      const firstvue = new Folder(
        "First Vue project",
        "project",
        "p",
        [],
        "",
        false,
        true,
        "First VUE project",
        firstvuedesc,
        firstvuetech,
        "https://github.com/jonas2422/interface-exam",
        "http://firstvue.jonas95.com"
      );

      const oldWebSite = new Folder(
        "Old_website",
        "explorer",
        "l",
        [],
        "",
        false,
        true,
        "",
        "",
        "",
        "",
        "",
        "http://portfolio.jonas95.com/"
      );
      const pdf = new Folder(
        "Business Kolding.pdf",
        "pdf",
        "l",
        [],
        "",
        false,
        true,
        "",
        "",
        "",
        "",
        "",
        "../letter-of-recommendation-jonas-tamosevicius.pdf"
      );

      const test = new Folder(
        "My_Portfolio",
        "folder",
        "f",
        [
          InternetIdeas,
          musudarbas,
          project,
          startup,
          gviskudzoo,
          portfolio,
          politics,
          firstvue,
          codeddesign
        ],
        "",
        true,
        true
      );
      const recommendation = new Folder("Reference letters", "folder", "f", [
        pdf
      ]);
      const trash = new Folder("Recycle bin", "bin", "f", [oldWebSite]);
      const text = new Folder("About_me.txt", "text", "t", [], aboutMe);
      const hire = new Folder(
        "Hire_me.txt",
        "text",
        "t",
        [],
        "Currently I am available for Front end, Back end or for Full stack web developer position, please contact me through email: gerulisjonas1@gmail.com ."
      );

      const mail = new Folder("Contact", "mail", "m", [], "", true, true);
      const cv = new Folder(
        "CV.pdf",
        "pdf",
        "l",
        [],
        "",
        true,
        true,
        "",
        "",
        "",
        "",
        "",
        "../Jonas_Tamosevicius_CV.pdf"
      );

      const linkedin = new Folder(
        "My LinkedIn profile",
        "linkedin",
        "l",
        [],
        "",
        false,
        true,
        "",
        "",
        "",
        "",
        "",
        "https://www.linkedin.com/in/jonastamosevicius/"
      );
      const github = new Folder(
        "My GitLab",
        "github",
        "l",
        [],
        "",
        true,
        false,
        "",
        "",
        "",
        "",
        "",
        "https://gitlab.com/tamoseviciusj/"
      );

      return [
        test,
        trash,
        recommendation,
        text,
        mail,
        cv,
        hire,
        linkedin,
        github
      ];
    }
    this.globalnav.push(programs());
    this.receiver(programs());
  }
};
